#include "Feal.h"
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

unsigned char* toByte(string in)//++
{
    unsigned char* v = new unsigned char[in.length()];
    strcpy((char*)v, in.c_str());
    return v;
}

string toString(unsigned char* in)//++
{
    return string(reinterpret_cast<char*>(in));
}
void write(string name, const vector<unsigned char>in){
    FILE *output;
    if ((output=fopen(name.c_str(), "wb"))==NULL) {
        printf ("Cannot open file.\n");
        return;
    }
    unsigned char t;
    for(int i = 0; i < in.size(); ++i){
        t = in[i];
        fwrite(&t,sizeof(t),1,output);
    }
    fclose(output);
} 

vector<unsigned char> read(string name){
    FILE *input;
    if ((input=fopen(name.c_str(), "rb"))==NULL) {
        printf ("Cannot open file.\n");
    }
    vector<unsigned char> res;
    unsigned char t;
    while(!feof(input)){
        fread(&t,sizeof(t),1,input);
        res.push_back(t);
    }
    fclose(input);
    return res;
}

int main(int argc, char* argv[]) {
    string argvs[] = {"-pin", "-pout", "-k", "-h", "-s", "-d", "-e"};
    bool usedArgvs[7]  = {false};
    unsigned char key[8];
    string filein, fileout;
    for(int i = 0; i < argc; ++i){
        for(int j = 0; j < 7; ++j){
            if(argv[i] == argvs[j]){
                usedArgvs[j] = true;
                if(j == 0){
                    filein = argv[i + 1];
                }
                if(j == 1){
                    fileout = argv[i + 1];
                }
                if(j == 2){
                    for(int k = 0; k < block_size; ++k){
                        key[k] = (unsigned char)atoi(argv[i + k + 1]);
                    }
                }
                if(j == 3){
                    cout<<"Help:\n\t-p_in input file"<<endl;
                    cout<<"\t-p_out output file"<<endl;
                    cout<<"\t-k input key: 8 ints in the range 0 - 255"<<endl;
                    cout<<"\t-s show generated keys"<<endl;
                    cout<<"\t-d decrypt"<<endl;
                    cout<<"\t-e encrypt"<<endl;
                    cout<<"\t-h help"<<endl;
                    return 2;
                }
            }
        }
    }
    if(usedArgvs[0]){
        vector<unsigned char> in = read(filein);
        vector<unsigned char> res;
        if(usedArgvs[2]){
            Feal feal(key, sizeof(key), usedArgvs[4]);
            if(usedArgvs[5]){
                res = feal.decrypt(in.data(), in.size());
            }
            if(usedArgvs[6]){
                res = feal.encrypt(in.data(), in.size());
            }
        }        
        else{
            Feal feal(usedArgvs[4]);
            if(usedArgvs[5]){
                res = feal.decrypt(in.data(), in.size());
            }
            if(usedArgvs[6]){
                res = feal.encrypt(in.data(), in.size());
            }
        }
        if(usedArgvs[1]){
            write(fileout, res);
        }
        return 1;
    }
    
    
    
}