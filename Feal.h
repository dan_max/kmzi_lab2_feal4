#ifndef feal_h
#define feal_h

#include <iostream>
#include <string.h>
#include <vector>
#include <ctime>

#define block_size 8

using namespace std;

class Feal
{
private:
    unsigned char** keys;
    void generateKeys(unsigned char*, bool, bool);
    unsigned char s0(unsigned char, unsigned char);
    unsigned char s1(unsigned char, unsigned char);
    unsigned char* fk(unsigned char*, unsigned char*);
    unsigned char* f(unsigned char* a, unsigned char* key);
    unsigned char* roundKey(unsigned char*, unsigned char*, unsigned char*);
    unsigned char* roundKey(unsigned char*, unsigned char*);
    unsigned char* encrypt_block(unsigned char* in);
    unsigned char* decrypt_block(unsigned char* in);
    void corr(const unsigned char* in, int size);
    int* toBin(unsigned char in);
    void kcorr(const unsigned char* first,const unsigned char* second, int size);
public:
    vector<unsigned char> encrypt(const unsigned char*, int);
    vector<unsigned char> decrypt(const unsigned char*, int);
    unsigned char* randomKey();
    Feal(bool);
    Feal(unsigned char*, unsigned int, bool);
    ~Feal();
};

#endif
