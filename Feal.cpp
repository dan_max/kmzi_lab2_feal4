#include "Feal.h"



unsigned char* Feal::encrypt_block(unsigned char *in){
    unsigned char* l = new unsigned char[block_size / 2];
    unsigned char* r = new unsigned char[block_size / 2];
    unsigned char* res = new unsigned char[block_size];
    unsigned char* t;
    in[0] = in[0] ^ keys[8][0];
    in[1] = in[1] ^ keys[8][1];
    in[2] = in[2] ^ keys[9][0];
    in[3] = in[3] ^ keys[9][1];
    in[4] = in[4] ^ keys[10][0];
    in[5] = in[5] ^ keys[10][1];
    in[6] = in[6] ^ keys[11][0];
    in[7] = in[7] ^ keys[11][1];
    for (int j = 0; j < block_size / 2; ++j) {
        l[j] = in[j];
        r[j] = in[j + block_size / 2];
    }
    for (int j = 0; j < block_size / 2; ++j) {
        l[j] = r[j] ^ l[j];
    }
    for (int c = 0; c < 8; ++c) {
        t = f(r, keys[c]);
        for (int j = 0; j < block_size / 2; ++j) {
            l[j] = l[j] ^ t[j];
        }
        delete[] t;
        t = l;
        l = r;
        r = t;
    }
    for (int j = 0; j < block_size / 2; ++j) {
        r[j] = r[j] ^ l[j];
    }
    for (int j = 0; j < block_size / 2; ++j) {
        res[j] = l[j];
        res[j + block_size / 2] = r[j];
    }
    res[0] = res[0] ^ keys[12][0];
    res[1] = res[1] ^ keys[12][1];
    res[2] = res[2] ^ keys[13][0];
    res[3] = res[3] ^ keys[13][1];
    res[4] = res[4] ^ keys[14][0];
    res[5] = res[5] ^ keys[14][1];
    res[6] = res[6] ^ keys[15][0];
    res[7] = res[7] ^ keys[15][1];
    delete[] l;
    delete[] r;
    return res;    
}

unsigned char* Feal::decrypt_block(unsigned char *in){
    unsigned char* l = new unsigned char[block_size / 2];
    unsigned char* r = new unsigned char[block_size / 2];
    unsigned char* res = new unsigned char[block_size];
    unsigned char* t;
    in[0] = in[0] ^ keys[12][0];
    in[1] = in[1] ^ keys[12][1];
    in[2] = in[2] ^ keys[13][0];
    in[3] = in[3] ^ keys[13][1];
    in[4] = in[4] ^ keys[14][0];
    in[5] = in[5] ^ keys[14][1];
    in[6] = in[6] ^ keys[15][0];
    in[7] = in[7] ^ keys[15][1]; 
    for (int j = 0; j < block_size / 2; ++j) {
        l[j] = in[j];
        r[j] = in[j + block_size / 2];
    }    
    for (int j = 0; j < block_size / 2; ++j) {
        r[j] = r[j] ^ l[j];
    }
    t = r;
    r = l;
    l = t;
    for (int c = 7; c >= 0; --c) {
        t = f(r, keys[c]);
        for (int j = 0; j < block_size / 2; ++j) {
            l[j] = l[j] ^ t[j];
        }
        delete[] t;
        t = l;
        l = r;
        r = t;
    }
    t = l;
    l = r;
    r = t;
    for (int j = 0; j < block_size / 2; ++j) {
        l[j] = r[j] ^ l[j];
    }    
    for (int j = 0; j < block_size / 2; ++j) {
        res[j] = l[j];
        res[j + block_size / 2] = r[j];
    }
    res[0] = res[0] ^ keys[8][0];
    res[1] = res[1] ^ keys[8][1];
    res[2] = res[2] ^ keys[9][0];
    res[3] = res[3] ^ keys[9][1];
    res[4] = res[4] ^ keys[10][0];
    res[5] = res[5] ^ keys[10][1];
    res[6] = res[6] ^ keys[11][0];
    res[7] = res[7] ^ keys[11][1];
    delete[] l;
    delete[] r;
    return res;    
}

vector<unsigned char> Feal::encrypt(const unsigned char* in, int size){
    int k, add_size = 0;
    unsigned char* inc;
    bool is_first = true;
    add_size = size % block_size;
    if (add_size != 0){
        add_size = block_size - add_size;
    }
    if (size % block_size != 0) {
        inc = new unsigned char[size + add_size];
        for(int i = 0; i < size + add_size; ++i){
            if(i < size){
                inc[i] = in[i];
            }
            else{
                if(is_first){
                    inc[i] = 128;
                    is_first = false;
                }
                else{
                    inc[i] = 0; 
                }
            }
        }
    }
    else{
        inc = new unsigned char[size];
        for (int i = 0; i < size; ++i) {
            inc[i] = in[i];
        }  
    }
    corr(inc, size + add_size);
    k = (size + add_size) / block_size;
    int curr = 0;
    vector<unsigned char> res;
    for(int i = 0; i < k; ++i){
        unsigned char* temp = new unsigned char[block_size];
        for(int j = 0; j < block_size; ++j){
            temp[j] = inc[curr + j];
        }
        unsigned char* res_block = encrypt_block(temp);
        for(int j = 0; j < block_size; ++j){
            res.push_back(res_block[j]);
        }
        curr += block_size;
        delete[] temp;
        delete[] res_block;
    }
    corr(res.data(), res.size());
    kcorr(inc, res.data(), res.size());
    delete[] inc;
    return res;
}
int* Feal::toBin(unsigned char in){
    int* res = new int[8];
    for (int i = 7; i >= 0; --i){
        res[i] = ((in>>i)&1);
    }
    return res;
}
void Feal::kcorr(const unsigned char* first,const unsigned char* second, int size){
    int con_count = 0;
    double k;
    int* firstbin;
    int* secondbin;
    for(int i = 0; i < size; ++i){
        firstbin = toBin(first[i]);
        secondbin = toBin(second[i]);
        for(int j = 0; j < 8; ++j){
            if(firstbin[j] == secondbin[j]){
                con_count++;
            }
        }
        delete[] firstbin;
        delete[] secondbin;
    }
    k = (double)con_count/(double)(size * 8);
    if(k == 0){
        cout << "koef korr = " << k << endl;
    }
    else{
        cout << "koef korr = +-" << k << endl;    
    }
    
}

void Feal::corr(const unsigned char*in, int size){
    int count_ones = 0;
    int *temp;
    for(int i = 0; i < size; ++i){
        temp = toBin(in[i]);
        for(int j = 0; j < 8; ++j){
            if(temp[j] == 1){
                count_ones++;
            }
        }
        delete[] temp;
    }
    cout << "1/0 = " << count_ones << "/" << 8 * size - count_ones << endl;
}

vector<unsigned char> Feal::decrypt(const unsigned char* in, int size)
{
    int k;
    vector<unsigned char> res;
    unsigned char* inc;
    k = size / block_size;
    inc = new unsigned char[size];
    for (int i = 0; i < size; ++i) {
        inc[i] = in[i];
    }  
    int curr = 0;
    unsigned char* res_block;
    for(int i = 0; i < k; ++i){
        unsigned char* temp = new unsigned char[block_size];
        for(int j = 0; j < block_size; ++j){
            temp[j] = inc[curr + j];
        }
        res_block = decrypt_block(temp);
        for(int j = 0; j < block_size; ++j){
            res.push_back(res_block[j]);
        }
        curr += block_size;
        delete[] temp;
        delete[] res_block;
    }
    vector<unsigned char> resv;
    int i = res.size(), add = 0;
    
    while(res[i] != 128){
        add++;
        i--;
    }
    for(int i = 0; i < res.size() - add - 1; ++i){
        resv.push_back(res[i]);
    }    
    return resv;
}

void Feal::generateKeys(unsigned char* v, bool need_delete, bool show_keys)
{
    keys = new unsigned char* [16];
    for (int i = 0; i < 16; ++i) {
        keys[i] = new unsigned char[2];
    }
    unsigned char *a = new unsigned char[4];
    unsigned char *b = new unsigned char[4];
    for (int i = 0; i < 4; ++i) {
        a[i] = v[i];
    }
    for (int i = 0; i < 4; ++i) {
        b[i] = v[i + 4];
    }
    unsigned char *d = new unsigned char[4];
    for (int i = 0; i < 4; ++i) {
        d[i] = a[i];
    }
    unsigned char* y = roundKey(a, b);
    keys[0][0] = y[0];
    keys[0][1] = y[1];
    keys[1][0] = y[2];
    keys[1][1] = y[3];
    delete[]a;
    a = y;
    unsigned char *t;
    t = a;
    a = b; 
    b = t;
    int curr_key = 2;
    for (int i = 0; i < 7; ++i) {
        y = roundKey(a, b, d);
        keys[curr_key][0] = y[0];
        keys[curr_key][1] = y[1];
        keys[curr_key + 1][0] = y[2];
        keys[curr_key + 1][1] = y[3];
        delete[]a;
        a = y;
        t = a;
        a = b; 
        b = t;
        d[0] = b[0];
        d[1] = b[1];
        d[2] = b[2];
        d[3] = b[3];
        curr_key += 2;
    }
    if (show_keys) {
        cout << "key" << endl;
        for (int i = 0; i < 8; ++i) {
            cout << (int) v[i] << " ";
        }
        cout << endl;
        cout << "generated keys" << endl;
        for (int i = 0; i < 16; ++i) {
            cout << "key[" << i << "] ";
            for (int j = 0; j < 2; ++j) {
                cout << (int)keys[i][j] << " ";
            }
            cout << endl;
        }
    }
    if (need_delete) {
        delete[] v;
    }
    delete[] a;
    delete[] b;
    delete[] d;
}

unsigned char Feal::s0(unsigned char a, unsigned char b)//++
{
    return (((((int)a + (int)b) % 256) << 2) | ((((int)a + (int)b) % 256) >> 6));
}

unsigned char Feal::s1(unsigned char a, unsigned char b)//++
{
    return (((((int)a + (int)b + 1) % 256) << 2) | ((((int)a + (int)b + 1) % 256) >> 6));
}

unsigned char* Feal::fk(unsigned char *a, unsigned char *b)//++
{
    unsigned char *y = new unsigned char[4];
    y[1] = s1(a[0] ^ a[1], (a[2] ^ a[3]) ^ b[0]);
    y[0] = s0(a[0], y[1] ^ b[2]);
    y[2] = s0(a[2] ^ a[3], y[1] ^ b[1]);
    y[3] = s1(a[3] , y[2] ^ b[3]);
    return y;	
}

unsigned char * Feal::f(unsigned char *a , unsigned char *key)//++
{
    unsigned char* res = new unsigned char[4];               
    res[1] = s1((a[2] ^ key[1]) ^ a[3], (a[1] ^ key[0]) ^ a[0]);
    res[0] = s0(res[1], a[0]);
    res[2] = s0(res[1], (a[2] ^ key[1]) ^ a[3]);
    res[3] = s1(res[2], a[3]);
    return res;
}

unsigned char * Feal::roundKey(unsigned char *a, unsigned char *b)//++
{
    unsigned char *y = fk(a,b);
    return y;
}

unsigned char * Feal::roundKey(unsigned char *a, unsigned char *b, unsigned char *d)//++
{
    unsigned char* t = new unsigned char[4];
    for (int i = 0; i < 4; i++) {
        t[i] = b[i] ^ d[i];
    }
    unsigned char *y = fk(a,t);
    delete[] t;
    return y;
}

unsigned char* Feal::randomKey()//++
{
    unsigned char* v = new unsigned char[block_size];
    srand(time(0));
    for (int i = 0; i < block_size; ++i) {
	v[i] = rand() % 256;
    }
    return v;
}

Feal::Feal(unsigned char* in, unsigned int size, bool show_keys)//++
{
    if (size == block_size) {
	generateKeys(in, false, show_keys);
    }
    else {
        return;
    }
}

Feal::Feal(bool show_keys)//++
{
    generateKeys(randomKey(), true, show_keys);
}


Feal::~Feal()//++
{
    for (int i = 0; i < 16; i++) {
        delete[] keys[i];
    }
    delete[] keys;
}
